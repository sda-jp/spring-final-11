package pl.sda.jp.eventus11.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
public class WebappSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private PasswordEncoder passwordEncoder; //musieliśmy stworzyć takiego Beana, aby móc go wstrzyknąc (jego definicja jest nawet w tej klasie)

    @Autowired
    private DataSource dataSource;
    // Potrzebujemy tego pod autentykacje w oparciu o JDBC.
    // Poniewaz mamy tylko jeden datasource w naszej aplikacji, Spring będzie umiał go bez problemu wstrzyknac bez ręcznego jego definiowania.

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/event/add").hasAnyRole("USER")
                //.antMatchers("/event/add").hasAnyAuthority("ROLE_USER")
                .anyRequest().permitAll()
                .and().csrf().disable()
                .headers().frameOptions().disable()
                .and()
                .formLogin()
                    .loginPage("/login")
                    .usernameParameter("username")
                    .passwordParameter("password")
                    .failureUrl("/login?status=err")
                    .defaultSuccessUrl("/")

        ;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .passwordEncoder(passwordEncoder)
                .dataSource(dataSource)
                .usersByUsernameQuery("SELECT u.login, u.password, 1 FROM user u WHERE u.login = ?")
                .authoritiesByUsernameQuery("SELECT u.login, r.role_name FROM user u JOIN user_roles ur ON u.id = ur.user_id JOIN role r ON ur.roles_id = r.id WHERE u.login = ?");
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
