package pl.sda.jp.eventus11;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Eventus11Application {

    public static void main(String[] args) {
        SpringApplication.run(Eventus11Application.class, args);
    }
}
