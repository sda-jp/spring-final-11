package pl.sda.jp.eventus11.event.entities;

import lombok.Getter;
import lombok.Setter;
import pl.sda.jp.eventus11.infrastructure.AuditEntity;
import pl.sda.jp.eventus11.user.User;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Comment extends AuditEntity {

    @Column(length = 500)
    private String commentBody;

    @ManyToOne
    private Event event;

    @ManyToOne
    private User user;
}
