package pl.sda.jp.eventus11.event.entities;

import pl.sda.jp.eventus11.infrastructure.BaseEntity;
import pl.sda.jp.eventus11.user.User;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class EventRegistration extends BaseEntity {

    @ManyToOne
    private Event event;
    @ManyToOne
    private User user;
}
