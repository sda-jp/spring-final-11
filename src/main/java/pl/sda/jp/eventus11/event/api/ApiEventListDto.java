package pl.sda.jp.eventus11.event.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ApiEventListDto {
    private Long id;
    private String name;
    private Date startDate;
    @JsonIgnore
    private Date endDate;
}
