package pl.sda.jp.eventus11.event.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.sda.jp.eventus11.event.api.ApiEventDto;
import pl.sda.jp.eventus11.event.api.ApiEventListDto;
import pl.sda.jp.eventus11.event.entities.Event;
import pl.sda.jp.eventus11.event.repositories.EventRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/api")
public class EventRestController {

    private EventRepository eventRepository;

    @Autowired
    public EventRestController(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }


    @GetMapping("/events")
    public ResponseEntity<List<ApiEventListDto>> getEvents(){

        List<ApiEventListDto> events = eventRepository.findAll()
                .stream()
                //.filter(e -> e.getStartDate().after(new Date()))
                .map(e -> {
                    ApiEventListDto apiEventListDto = new ApiEventListDto();
                    apiEventListDto.setId(e.getId());
                    apiEventListDto.setName(e.getEventName());
                    apiEventListDto.setStartDate(e.getStartDate());
                    apiEventListDto.setEndDate(e.getEndDate());
                    return apiEventListDto;
                })
                .collect(toList());


        return ResponseEntity.ok(events);
    }

    @GetMapping("/event/{id}")
    public ResponseEntity<ApiEventDto> getEvent(@PathVariable Long id){

        Optional<ApiEventDto> apiEventDto = eventRepository.findById(id)
                .map(e -> {
                    ApiEventDto dto = new ApiEventDto();
                    dto.setId(e.getId());
                    dto.setName(e.getEventName());
                    dto.setStartDate(e.getStartDate());
                    dto.setEndDate(e.getEndDate());
                    dto.setDescritpion(e.getDescription());

                    dto.setComments(e.getComments().stream()
                            .map(c -> new CommentDto(c.getUser().getDisplayName(), c.getAdded(), c.getCommentBody()))
                            .collect(toList())
                    );

                    return dto;
                });


//        Event event = eventRepository.findById(id).get();
//        if(event != null){
//            String description = event.getDescription();
//            if(description != null){
//                int length = description.length();
//            }
//        }
//
//        Integer integer = eventRepository.findById(id)
//                .map(e -> e.getDescription())
//                .map(d -> d.length())
//                .orElse(0);
//

        if(apiEventDto.isPresent()){
            return ResponseEntity.ok(apiEventDto.get());
        } else {
            //return ResponseEntity.notFound().build();
            return ResponseEntity.status(404).body(null);
        }

    }


}
