package pl.sda.jp.eventus11.event.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ApiEventDto {
    private Long id;
    private String name;
    private Date startDate;
    private Date endDate;
    private String descritpion;
    private List<CommentDto> comments;
}
