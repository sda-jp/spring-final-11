package pl.sda.jp.eventus11.event.forms;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
public class AddEventFormDTO {
    @NotBlank
    private String eventName;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    private Date startDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;
    @Size(min = 20)
    @NotBlank
    private String description;
}
