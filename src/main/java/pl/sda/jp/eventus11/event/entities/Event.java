package pl.sda.jp.eventus11.event.entities;

import lombok.Getter;
import lombok.Setter;
import pl.sda.jp.eventus11.infrastructure.BaseEntity;
import pl.sda.jp.eventus11.user.User;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
public class Event extends BaseEntity {

    private String eventName;
    private Date startDate;
    private Date endDate;
    @Column(length = 4000)
    private String description;

    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "event")
    private List<Comment> comments;
}
