package pl.sda.jp.eventus11.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sda.jp.eventus11.event.forms.AddEventFormDTO;
import pl.sda.jp.eventus11.event.services.EventService;

import javax.validation.Valid;

@Controller
public class EventController {

    private EventService eventService;

    @Autowired
    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    //1. Wyświetlenie strony/formularza
    // - wymyslenie URLa - GET
    @GetMapping("/event/add")
    public String showAddEventForm(Model model) {
        // - stworzenie HTML'a
        // - może się okazac, że do HTML'a potrzebujemy Model
        model.addAttribute("addEventFormDTO", new AddEventFormDTO());
        return "event/addEventForm";
    }

    //2. Proces przyjmowania formularza, czyli obsługa formularza
    // - wymyslenie URLa - POST
    @PostMapping("/event/add") //to jest to samo co w action i method formularza w HTML <form ...>
    public String handleAddEventForm(@ModelAttribute @Valid AddEventFormDTO addEventFormDTO, BindingResult bindingResult
            , Authentication authentication, RedirectAttributes redirectAttributes) {

        //2aa Zabezpieczyć URL'a (lub kontroler) w Spring Security, jesli potrzeba

        //2a. Weryfikacja danych w formularzu
        // - określamy w obiekcie DTO ograniczenia/wymagania - adnotacje w obiekcie DTO
        // - dodajemy BindingResulat, zaraz za polem formularza (WAŻNE!) - jest to worek na błedy walidacji
        // - wymuszenie walidacji formularza, poprzez dodanie adnotacji @Valid
        // - zwrócenie wyniku walidacji do użytkownika, jęsli sa błedy
        // -- obsługa błedów w HTML - th:errors (np.: <span th:errors="*{description}"></span>)
        // -- sprawdzenie w kotrolerze i zwrócenie tego samego widoku co dla GET
        if(bindingResult.hasErrors()){
            return "event/addEventForm";
        }
        String login = authentication.getName();
        eventService.addNewEvent(addEventFormDTO, login);

        //2e. Wyswietlic potwierdzenie obsługi
        redirectAttributes.addFlashAttribute("msg", "Wydarzenie zostało dodane!");
        return "redirect:/";
    }



}
