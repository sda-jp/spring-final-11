package pl.sda.jp.eventus11.event.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.jp.eventus11.event.entities.Event;
import pl.sda.jp.eventus11.event.forms.AddEventFormDTO;
import pl.sda.jp.eventus11.event.repositories.EventRepository;
import pl.sda.jp.eventus11.user.User;
import pl.sda.jp.eventus11.user.UserRepository;


@Service
public class EventService {

    private EventRepository eventRepository;
    private UserRepository userRepository;
    private AddEventFormDTOToEventMapper addEventFormDTOToEventMapper;

    @Autowired
    public EventService(EventRepository eventRepository, UserRepository userRepository, AddEventFormDTOToEventMapper addEventFormDTOToEventMapper) {
        this.eventRepository = eventRepository;
        this.userRepository = userRepository;
        this.addEventFormDTOToEventMapper = addEventFormDTOToEventMapper;
    }

    public void addNewEvent(AddEventFormDTO addEventFormDTO, String login) {
        Event event = addEventFormDTOToEventMapper.convertDTO2Event(addEventFormDTO);

        //2c Spełnienie relacji między encjami
        User user = userRepository.findByLogin(login).get();
        event.setUser(user);

        //2d. Zeskładowanie teczki
        eventRepository.save(event);
    }

}
