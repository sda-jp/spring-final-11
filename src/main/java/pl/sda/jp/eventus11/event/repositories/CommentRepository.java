package pl.sda.jp.eventus11.event.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.jp.eventus11.event.entities.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long> {
}
