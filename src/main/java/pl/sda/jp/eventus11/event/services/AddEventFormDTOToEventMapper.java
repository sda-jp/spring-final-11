package pl.sda.jp.eventus11.event.services;

import org.springframework.stereotype.Service;
import pl.sda.jp.eventus11.event.entities.Event;
import pl.sda.jp.eventus11.event.forms.AddEventFormDTO;

@Service
public class AddEventFormDTOToEventMapper {
    public Event convertDTO2Event(AddEventFormDTO addEventFormDTO) {
        //2b. Konwersja formularza na format składowania danych - teczka wydarzenia
        Event event = new Event();
        event.setEventName(addEventFormDTO.getEventName());
        event.setStartDate(addEventFormDTO.getStartDate());
        event.setEndDate(addEventFormDTO.getEndDate());
        event.setDescription(addEventFormDTO.getDescription());
        return event;
    }
}
