package pl.sda.jp.eventus11.event.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.jp.eventus11.event.entities.Event;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
}
