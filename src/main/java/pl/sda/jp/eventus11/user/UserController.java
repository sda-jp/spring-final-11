package pl.sda.jp.eventus11.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class UserController {

    private UserRegistrationService userRegistrationService;

    @Autowired
    public UserController(UserRegistrationService userRegistrationService) {
        this.userRegistrationService = userRegistrationService;
    }

    @GetMapping("/register")
    public String showRegisterForm(Model model){
        model.addAttribute("registerForm", new RegisterForm());
        return "user/registerForm";
    }

    @PostMapping("/register")
    public String handleRegisterForm(@ModelAttribute @Valid RegisterForm registerForm, BindingResult bindingResult, RedirectAttributes redirectAttributes){
        System.out.println(registerForm);
        if(bindingResult.hasErrors()){
            return "user/registerForm";
        }

        try {
            userRegistrationService.registerNewUserWithDefaultRole(registerForm);
        } catch (UserExistsException e) {
            bindingResult.rejectValue("login", "user.existx", "Uzytkownik już istnieje!");
            return "user/registerForm";
        }

        redirectAttributes.addFlashAttribute("msg", "Dziękówka za rejestrację!");
        return "redirect:/";
    }

    @GetMapping("/login")
    public String showLoginForm(){
        return "user/loginForm";
    }
}
