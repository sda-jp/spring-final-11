package pl.sda.jp.eventus11.user;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@ToString(exclude = {"password"})
public class RegisterForm {
    @Email
    @NotBlank
    private String login;
    @Size(min = 8, max = 30)
    private String password;
    @NotBlank
    @Size(max = 50)
    private String displayName;
}
