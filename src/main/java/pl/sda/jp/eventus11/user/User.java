package pl.sda.jp.eventus11.user;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String login;
    @Column(length = 100)
    private String password;
    @Column(length = 50)
    private String displayName;
    @ManyToMany
    private Set<Role> roles;

}
