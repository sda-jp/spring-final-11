package pl.sda.jp.eventus11.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class UserRegistrationService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserRegistrationService(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void registerNewUserWithDefaultRole(RegisterForm registerForm) throws UserExistsException{
        userRepository
                .findByLogin(registerForm.getLogin())
                .ifPresent(u -> {throw new UserExistsException("user exists!");});

        User user = createNewUserWithDefaultRole(registerForm);

        userRepository.save(user);


    }

    private User createNewUserWithDefaultRole(RegisterForm registerForm) {
        User user = new User();
        user.setLogin(registerForm.getLogin());
        user.setDisplayName(registerForm.getDisplayName());
        user.setPassword(passwordEncoder.encode(registerForm.getPassword()));

        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository
                .findByRoleName(Role.ROLE_USER)
                //.orElse(roleRepository.save(new Role(Role.ROLE_USER))) //NIGDY TAK!!!!
                .orElseGet(() -> roleRepository.save(new Role(Role.ROLE_USER)))
        );


        user.setRoles(roles);
        return user;
    }
}
