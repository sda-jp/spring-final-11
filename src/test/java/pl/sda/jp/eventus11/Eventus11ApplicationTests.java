package pl.sda.jp.eventus11;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Eventus11ApplicationTests {

    @Test
    public void contextLoads() {
    }

}
